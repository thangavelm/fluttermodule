/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.example.fluttertestmodule;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.example.fluttertestmodule";
  public static final String BUILD_TYPE = "debug";
}
